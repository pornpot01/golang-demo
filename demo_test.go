package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

//$ go test
//จำเป็นต้องมี Test…. นําหน้านะและ function ต้องรับค่า *testing.T เข้ามาด้วย
func TestPlus(t *testing.T) {
	// result := Plus(1, 5)

	// if result != 6 {
	// 	t.Error("Expected 6, got : ", result)
	// }

	assert.Equal(t, 123, 123, "they should be equal")

	// assert equality
	result := Plus(1, 5)
	assert.Equal(t, result, 6, "they should be equal")
}
