FROM alpine:3.8
LABEL name="Golang Demo" \
    version="1.0.0" \
    org.label-schema.vcs-url="https://gitlab.com/pornpot01/golang-demo" \
    org.label-schema.vendor="Twin synergy"
RUN apk --no-cache add ca-certificates \
    && apk add --update tzdata \
    && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
    && apk del tzdata
COPY main /usr/local/bin/app
EXPOSE 8081
CMD ["/usr/local/bin/app"]
