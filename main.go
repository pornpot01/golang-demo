// Echo Framework
// go get -u github.com/labstack/echo
// go get -u github.com/labstack/echo/engine/fasthttp

// ประกาศ package ชื่อ main
package main

// นำเข้า package แต่ละตัวมาใช้งาน
import (
	"log"
	"net/http"

	_ "github.com/gorilla/mux"
)

var port = "8081"

// ฟังก์ชัน Main จะฟังก์ชันเริ่มต้นของโปรเจค
func main() {
	//println("Hello GOLANG")

	router := NewRouter()
	log.Fatal(
		// start on port 3000 by default
		http.ListenAndServe(":"+port, router),
	)

	//log.Fatal(http.ListenAndServe(":"+port, router()))
}

func Plus(a, b int) int {
	return a + b + 3
}
